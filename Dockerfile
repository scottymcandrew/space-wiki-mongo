FROM mongo

WORKDIR /

ENV MONGO_INITDB_DATABASE=space_definitions
ENV MONGO_INITDB_ROOT_USERNAME=space_user
ENV MONGO_INITDB_ROOT_PASSWORD=jupiter


COPY init-mongo.js /docker-entrypoint-initdb.d/.

EXPOSE 27017